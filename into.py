r = int(raw_input('Ingrese el radio: '))
pi = 3.14

if r < 0:
	print "El valor ingresado debe ser positivo."
	r = int(raw_input('Ingrese el radio nuevamente: '))
else:
	area = pi *(r**2)

print 'El area de una circunferencia de radio', r, 'es', area