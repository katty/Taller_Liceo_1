## Asignación

```python
variable = expresión
```

## Entrada

```python
variable = tipo(raw_input('Ingrese valor de variable: '))
```

## Salida

```python
>>> print variable
variable
>>> print 'Hola a todos'
'Hola a todos'
>>> print 23
23
```

## Comentarios

```python
>>> #Esto es un comentario corto
```

# _Sentencias de control_

Las sentencias son todas aquellas instrucciones que el programa ejecuta. Existen las simples, que son de una línea como

```python
>>> a = 8
>>> b = 20
>>> suma = a + b
```

Y existen las sentencias de control, que podemos separar en Condicionales y Ciclos.

## Condicional `If - elif - else ` _(si - o si no)_

Esta sentencia, se ejecuta solamente si cumple la condición que le entregamos. 

```python
if condición:
	sentencia
```
Ejemplos:
```python
# Ejemplo 1. Estado de la toma
estado = str(raw_input('Ingrese estado de la toma: '))

# Podemos tener 3 tipos de estados. Desalojo, en toma y se baja.
if estado == 'desalojo':
	correr()
elif estado == 'en toma':
	seguir()
else:
	volver_a_clases()

#Ejemplo 2. Quintiles.
sueldo = int(raw_input('Ingrese el sueldo total de su familia: '))
inte = int(raw_input('Ingrese número de integrantes de su familia: '))

per_capita = sueldo / inte

if 0 < per_capita < 74969:
	print 'Eres del primer quintil'
elif 74970 < per_capita < 125558:
	print 'Eres del segundo quintil'
elif 125559 < per_capita < 193104:
	print 'Eres del tercer quintil'
else:
	print 'Eres del quinto quintil'
```

## Ciclo `while` _(mientras)_
Esta sentencia se cumple mientras se cumpla la condición que ingresamos.

```python
while condición:
	sentencia
```

Ejemplos:
```python
# Ejemplo 1. Multiplicar un número hasta 10.
num = int(raw_input('Ingrese numero: '))
cont = 0

while cont <= 10:
	mult = cont * num
	print num, 'x', cont, '=', mult
	cont = cont + 1
```

## Ciclo `for` _(para)_
Esta sentencia se cumple para un cierto rango.

```python
for variable in range(inicial, final, incremento):
	sentencia para cada valor de variable
```

Ejemplos:
```python
# Ejemplo 1. Multiplicar un número hasta 10.
num = int(raw_input('Ingrese numero: '))

for i in range(0, 11, 1):
	mult = i * num
	print num, 'x', i, '=', mult

#Ejemplo 2. Mostrar las letras de una frase.
frase = str(raw_input('Ingrese cualquier frase: '))

for i in range(len(frase)):
	print frase[i]

#Que es lo mismo que 
for i in frase:
	print i
```

![alt text][license]
[license]:file:///home/katty/Desarrollo/Taller%20L1/license.png "License"

