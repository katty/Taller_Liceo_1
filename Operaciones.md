# Expresiones


Son las variables, operadores y llamadas a funciones que ocupamos dentro del programa.

Ejemplo:

```python
>>> 8 + 1 - 9
0
>>> a = sqrt(4)
>>> a
2
```

### **Operadores**

Símbolo que representa una operación que se aplica a ciertos valores.

Operadores aritméticos:

- Suma `+` 

- Resta `-` (este mismo operando se utiliza para definir números negativos)

- Multiplicación `*`

- División `/`

- Módulo `%` (resto de una división)

- Potencia `**` (varía en algunos lenguajes)

- Raíz cuadrada `sqrt` (puede diferir en otro lenguaje)

```python
>>> 29 - 10
19
>>> 5.7 - 2
3.7
>>> 5.7 - 3.7  # Si uno de los operandos es un float, el resultado también será float
2.0
>>> 37 / 4
9
>>> 37 / 4.0
9.25
>>> 37 % 4  # Este operando nos ayuda a ver si un número es divisible de otro
1
>>> 37 % 4.0
1.0
```

Operadores relacionales: utilizadas para comparar valores, por lo tanto, sus resultados son siempre valores lógicos.

- Igual a `==` (que no es el mismo a `=` ocupado para asignar valores a variables)

- Distinto a `!=`

- Mayor que `>`

- Menor que `<`

- Mayor o igual que `>=`

- Menor o igual que `<=`

```python
>>> a = 2
>>> b = 10
>>> c = 23
>>> a < b
True
>>> b > c
False
>>> a + b == c
False
>>> a < 5 < b
True
>>> 4 < a < 10  # Equivalente a decir (a > 4) y (a < 10)
False
```

Operadores lógicos: 

- Y `and`

- O `or`

- No `not`

```python
>>> False == True
False
>>> False != True
True
>>> False == 0
True
>>> True == 1
True
```
Estos operadores siguen la misma lógica que la tabla de verdad:

| p | q | p and q | p or q |
| :---: | :---: | :---: | :---: |
| 1 | 1 |    1    |    1   |
| 1 | 0 |    0    |    1   |
| 0 | 1 |    0    |    1   |
| 0 | 0 |    0    |    0   |

Operadores de texto: en **Python** podemos operar textos y letras *como si fueran* números, de modo que la suma concatena cadenas de caracteres y la multiplicación los repite cuantas veces indiquemos.

```python
>>> 'Hola' + 'Javierinas'
'HolaJavierinas'
>>> 'Javierinas' * 3
'JavierinasJavierinasJavierinas'
>>> j = 'Javierinas'
>>> i = 'Hola'
>>> i + j
'HolaJavierinas'
>>> # Podemos pedir mostrar una letra de la palabra según su posición
>>> j[0]
'J'
>>> # Pedir el largo de la palabra
>>> len(j)
10
>>> # Comparar alfabéticamente palabras con "<" y ">"
>>> 'Javier' < 'Javierinas'
True
>>> # Verificar si una letra o cadena de caracteres está dentro de una palabra
>>> 'lele' in 'paralelepipedo'
True
>>> 'u' in 'paralelepipedo'
False
```

**El orden en el que se ejecutan los operadores es:**

- Operadores aritméticos (PAPOMUDAS)
- Operadores racionales
- Operadores lógico


Llamadas a funciones: las funciones en Python y en todos los lenguajes son muchas y muy variadas. Si bien hay algunas que se pueden encontrar en la mayoría de los lenguajes, hay algunas exclusivas para cada uno que ocupan funcionalidades específicas del lenguaje en el que estés trabajando.

Algunas de las funciones más comunes son:

- `len()`. Indica el largo de una cadena de caracteres.
- `max()`. Indica el máximo número de una lista de números.
- `min()`. Indica el mínimo número de una lista de números.
- `abs()`. Indica el absoluto de un número.

**_Funciones exclusivas de Python_**
```python
>>> int('2' + '8')
28
>>> str(4 * 2 + 5)
'13'
>>> float('3.95')
3.95
>>> a = '923'
>>> a = int(a)
>>> a
923
```
Son funciones exclusivas de python, ya que es un lenguaje que permite cambiar de tipo a sus variables ya guardadas.

![alt text][license]
[license]:file:///home/katty/Desarrollo/Taller%20L1/license.png "License"

