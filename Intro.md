# Taller introductorio a la programación

> Diseñar un programa es crear y describir procedimientos de forma tal que se resuelva completamente el problema.

### Algoritmos en la vida real:

*Receta de cocina.*

+ Entrada: ingredientes y cantidades.
+ Proceso: instrucciones de la receta para preparar el plato.
+ Salida: el plato terminado.

En teoría, si la receta está lo suficientemente bien explicada, cualquier persona sería capaz de preparar el plato, aunque no sepa cocinar.

Lo mismo pasa con el computador. Si el programa está correctamente escrito, cualquier computador debería ser capaz de leerlo y ejecutarlo.


## Partes de un programa

1.Entrada: son los datos sobre los que el algoritmo opera.
2.Proceso: son los pasos que se deben seguir. En este paso se utilizan los datos que definimos en la entrada.
3.Salida: resultado que entrega el algoritmo.

## Cómo escribir un algoritmo

1.Entrada:

```python
>>> # Asignación de variables
>>> a = 10 
>>> a
10
>>> b = 'Hola'
>>> b
'Hola'
>>> # Sobre escribir variables
>>> b = 'Liceo 1'
>>> b 
'Liceo 1'
```

### Tipos de variables:

- `int`: números enteros. Ej: 2, 6, -3, 20, etc.
- `float`: números reales. Ej: 0.65, 1.2, 65.23, etc.
- `char`: usado para letras. A menos que se indique lo contrario, puede contener una sola letra.
- `string`: usado para frases.
- `bool`: valores lógicos. Tiene sólo dos valores, True o False.

**En lenguajes como Python, que no son fuertemente tipados, no es necesario declarar los tipos a cada variable. En cambio, en lenguajes como C, C++ que sí lo son, cada vez que se declara una variable, se debe especificar su tipo.**

2.Proceso:

```python
# Imaginemos que queremos calcular el área de una circunferencia
r = int(raw_input('Ingrese el radio: ')) 
pi = 3.14

area = pi *(r**2)

print "El area de una circunferencia de radio " + str(r) + " es " + str(area)
```

Ahora, si el usuario ingresa un número negativo (aunque no indique diferencia alguna en el resultado final), sabemos que no es válido, porque los radios deben ser siempre positivos. Entonces:

```python
r = int(raw_input('Ingrese el radio: '))
pi = 3.14

if r < 0:
	print "El valor ingresado debe ser positivo."
	r = int(raw_input('Ingrese el radio nuevamente: '))
else:
	area = pi *(r**2)

print 'El area de una circunferencia de radio', r, 'es', area
```

3.Salida:

```python
[katty@localhost Taller L1]$ python into.py
Ingrese el radio: 8 
El area de una circunferencia de radio 8 es 200.96
```

![alt text][license]
[license]:file:///home/katty/Desarrollo/Taller%20L1/license.png "License"


